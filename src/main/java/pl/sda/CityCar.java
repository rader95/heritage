package pl.sda;

public class CityCar extends Car{
    int fuelLevel;
    public CityCar(String name, String color, float engineSize) {
        super(name,color,engineSize);
        fuelLevel = 20;
    }
}
