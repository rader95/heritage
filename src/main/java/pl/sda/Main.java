package pl.sda;

public class Main {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle("mercedes", "red");
        System.out.println(vehicle.name);
        Car car = new Car("bmw", "black");
        System.out.println(car.name);
        CityCar cityCar = new CityCar("citigo", "yellow", (float) 2.5);
        System.out.println(cityCar.getEngineSize());

    }
}
