package pl.sda;

public class Car extends Vehicle{
    public float getEngineSize() {
        return engineSize;
    }

    private float engineSize;
    public Car (String name, String color) {
        super(name,color);
        engineSize = (float) 1.5;
    }
    public Car(String name, String color, float engineSize) {
        this.name = name;
        this.color = color;
        this.engineSize = engineSize;
    }


}
